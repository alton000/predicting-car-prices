# Predicting Car Prices



## Getting started

### The libraries I used

I used the Jupyter-Notebook as my IDE: https://jupyter.org/try
Pandas to manipulate data: https://pandas.pydata.org/docs/
Scikit-Learn to make my predictions: https://scikit-learn.org/stable/

## The Data we have

We got a random car prices data in autos.csv.
It's got 21 columns, you can view all the columns on the pynb file.
The relevant columns I found there were:
*The prices column: it was our Y.
*The X categorical columns were:
-GearBox
-Model
-Brand
-FuelType

*The numerical columns were:
-Kilometer
-MonthOfRegistration
-yearOfRegistration
-PowerPS

## Pre-processing data
I transformed all the numerical columns into floats
And all the categorical type into 0 and 1 for each category.
I tried enumerating the categorical types and it wasn't very effective.

## Fitting the linear models
I tried about 8 linear models or more and only 4 stood out through Cross Validation:
-LassoCV
-ElasticNetCV
-RidgeCV
-Lars

I chose the CV versions for the first 3 models so they could automatically choose
the best parameters for each function.

## Finding the best model through Cross Validation

I used the standard scoring for scikit-learn, which, for regression problems, the best 
score is always the smallest score possible, because that score is the distance from 
the model's prediction with the Y. So if the score is 0, it means the model fitted
is predicting exactly Y.

I used a standard cross-validation with 5 parts.

The best scores were from:
-LassoCV
-ElasticNetCV

and the worst ones were:
-RidgeCV
-Lars.

## And that is it:
Thank you!

## Hire me:
this was one of my portfolio programs to be a Data Analyst, if you would like to hire me,
contact me here through gitlab. Thanks!

